Tezos Replay Transaction
==============================

The purpose of this tool is to give users the ability to replay historical
transactions to smart contracts on Tezos Blockchain. This is useful especially
for debugging Michelson, but also for testing.

Requirements
---------------

1. A `tezos-node` running the Tezos mainnet
2. `tezos-client` binary to execute requested transactions with
3. Internet access required to retrieve transaction details from [tzkt.io](https://tzkt.io).

Usage
------

The simplest use case is just to call the script giving it an account address:

    $ python replay-transaction.py onn9PgCdS7zQHoWxvbpqxEaaQogC1jM8hFq2uYEqhGjezKBPZPT
    
The script will fetch details of all operations targeting that account from the indexer and pass them to
local `tezos-client` binary in order to replay them. The full command executed
with `tezos-client` as well as its results will be printed on standard output.

Add `-c` option to have the script compare operations resulting from script's
execution to those that really happened on chain:

    $ python replay-transaction.py -c onn9PgCdS7zQHoWxvbpqxEaaQogC1jM8hFq2uYEqhGjezKBPZPT
    
Add `-q` option to suppress the standard output and print only a nice summary in
the form of Emacs ORG table.

Options
---------

Type:

    $ python replay-transaction.py --help
    
to see the following help:

    usage: replay-transaction.py [-h] [-o OPERATION] [--script SCRIPT] [--script-from-node] [--amount AMOUNT] [--balance BALANCE] [--source SOURCE] [--client CLIENT] [-m] [-n NODE] [-d CLIENT_DIR] [-t] [-c] [-q] target

    positional arguments:
    target                The address of the contract.

    options:
    -h, --help            show this help message and exit
    -o OPERATION, --operation OPERATION
                          An operation hash of the operation to execute.
    --script SCRIPT       A file containing script code to replace the original script.
    --script-from-node    Fetch script code from the node.
    --amount AMOUNT       The amount of Tez transferred to the contract account.
    --balance BALANCE     Balance of the contract account before the transaction.
    --source SOURCE       The source address of the transaction.
    --client CLIENT       The command to run as tezos-client.
    -m, --mockup          Run the client in mockup mode.
    -n NODE, --node NODE  URL of the node to connect to.
    -d CLIENT_DIR, --data-dir CLIENT_DIR
                          Client data directory.
    -t, --trace           Print stack state after each instruction.
    -c, --check           Compare obtained results with those stored on chain.
    -q, --quiet           Minimize the produced output to reporting errors.
    -f FILTER, --filter FILTER
                          Provide a function from operation to bool in order to filter some operations out.
                        
* `--script filename` – accepts a `filename`. The file should contain a
well-formed Michelson script to execute instead of the original one.

* `--script-from-node` – the script will be taken from the node's context.

* `--amount tez` – a floating number of Tez passed with the transaction.

* `--balance tez` – a floating number of Tez belonging to the smart contract
**before** the `amount` has been added to it.

* `--client` – a custom command to execute instead of the default `tezos-client`.
This is useful when the client is compiled from sources and placed in a non-
standard location. Note that the `TEZOS_CLIENT` environment variable will be
used as the default if it is defined.

* `--node` – a URL of the Tezos node to execute transactions on.

* `--trace` – displays script's stack transformations between successive
instructions.

* `--check` – verify the operations produced by script's execution (see below).

* `--filter` – input Python code producing a function from dict to bool. Only
operations tested positively with this function will be replayed. If used
together with `--operation`, only listed operations are tested against the
given function. For example: `--filter 'lambda op: int(op["level"]) > 1500000'`.

Known issues
---------------

Sometimes pieces of Michelson (scripts, storages, parameters) returned by the
indexer may be malformed. In particular there are scripts containing indentation
errors. For this reason this script tends to fetch JSON data (as this format is
more forgiving) and use the `tezos-client` to convert it to Michelson. This 
takes time and slows the script down, but is necessary in order to replay some
transactions.

Some Michelson scripts tend to be too long to fit in the command line. For this
reason the scripts passes it to the `tezos-client` via standard input. Thus
`run script /dev/stdin` in the displayed call to `tezos-client`. For this reason
the script isn't shown to the user unless the transaction fails (in which case
`tezos-client` will display it). Unfortunately, users have to fetch the script
from the indexer on their own if they need it.

Most contracts won't execute properly in mockup mode, in particular if they
contain or get account addresses as parameters. In those cases the contracts
must exist in the context or the script/storage/parameters won't type-check
otherwise. Since it's impossible to force contract origination to produce a
specific address, these contracts must be executed on the node containing
mainnet context.

Check history
----------------

The `check-history.sh` is a Bash script, which allows to replay *en masse*
the transactions to accounts listed in a file (or on STDIN). In that case
results are checked (`-c` option) and printed in quite mode (`-q` option).
More options can be given on the command line – they will be passed to each
call of `replay-transaction.py`.
