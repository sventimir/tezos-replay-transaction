#!/bin/env python
from argparse import ArgumentParser
import json
import os
import subprocess
import sys
import traceback
from urllib import request, parse
from functools import partial


HOME = os.getenv('HOME')
TZKT = 'https://api.tzkt.io/v1'
MICHELSON_UNIT = '{"prim": "Unit"}'
OP_FIELDS_TO_FETCH='id,hash,level,sender,target,amount,balance,storage,parameter,timestamp,status'

class Option:

    def __init__(self, value):
        self.val = value

    def map(self, f):
        return self if self.val is None else self.__class__(f(self.val))

    def bind(self, f):
        return self if self.val is None else f(self.val)

    @classmethod
    def some_if(cls, condition, value, default=None):
        ret = value if condition else default
        try:
            return ret()
        except TypeError:
            return ret

    def or_else(self, alternative):
        return self if self.val is not None else self.__class__(alternative).join()

    def value(self, default):
        if self.val is not None:
            return self.val
        else:
            try:
                return default()
            except TypeError:
                return default

    def join(self):
        return self.val.join() if isinstance(self.val, self.__class__) else self

    @classmethod
    def any_or_both(cls, left, right, combine):
        if left.val is None:
            return right
        elif right.val is None:
            return left
        else:
            return cls(combine(left.val, right.val))

    def __repr__(self):
        return 'NONE' if self.val is None else 'SOME<{}>'.format(self.val)

class Result:

    SUCCESS = 1
    FAILURE = 0

    def __init__(self, status, value, log=None):
        self.status = status
        self.value = value
        self._log = log

    @classmethod
    def ok(cls, value, log=None):
        return cls(cls.SUCCESS, value, log=log)

    @classmethod
    def fail(cls, value, log=None):
        return cls(cls.FAILURE, value, log=log)

    def map(self, f):
        return self.__class__(self.SUCCESS, f(self.value)) if self.status == self.SUCCESS \
            else self

    def join(self):
        status = self.SUCCESS \
            if self.status == self.SUCCESS and self.value.status == self.SUCCESS \
               else self.FAILURE
        return self.__class__(status, self.value.value)

    def bind(self, f):
        return f(self.value) if self.status == self.SUCCESS else self

    @classmethod
    def bind_many(cls, f, **results):
        args = {}
        for (k, r) in results.items():
            if r.status == cls.SUCCESS:
                args[k] = r.value
            else:
                return r
        return f(**args)

    def map_error(self, f):
        return self.__class__(cls.FAILURE, f(self.value), log=self._log) \
            if self.status == cls.FAILURE else self

    @classmethod
    def both(cls, fst, snd):
        return fst >> (lambda x: snd >> (lambda y: cls(cls.SUCCESS, (x, y))))

    def __repr__(self):
        return self.repr_success() if self.status == self.SUCCESS \
            else self.repr_error()

    def repr_success(self):
        return repr(self.value)

    def repr_error(self):
        return 'ERROR: ' + repr(self.value)

    def log(self):
        print(self._log, file=self._out_channel())

    @property
    def status_repr(self):
        return 'SUCCESS' if self.status == self.SUCCESS else 'FAILURE'

    def _out_channel(self):
        return sys.stdout if self.status == self.SUCCESS else sys.stderr

    def is_success(self):
        return self.status == self.SUCCESS

    def is_failure(self):
        return self.status == self.FAILURE

    # get monadic binding syntax similar to functional languages
    def __rshift__(self, f):
        return self.bind(f)


class ClientResult(Result):

    def __init__(self, status, value, cmd, log=None, input=None):
        Result.__init__(self, status, value, log=log)
        self.cmd = cmd
        self.input = input

    @classmethod
    def get(cls, cmd, input=None):
        PIPE = subprocess.PIPE
        input_bytes = Option(input).map(lambda i: i.encode('utf-8')).value(None)
        proc = subprocess.Popen(cmd, stdin=PIPE, stdout=PIPE, stderr=PIPE)
        (out, err) = proc.communicate(input=input_bytes)
        log = (out.decode('utf-8'), err.decode('utf-8'))
        if proc.wait(timeout=120) == 0:
            return cls(cls.SUCCESS, out.decode('utf-8'), cmd=cmd, input=input, log=log)
        else:
            return cls(cls.FAILURE, proc.returncode, cmd=cmd, log=log)

    def map(self, f):
        return self.__class__(self.SUCCESS, f(self.value), self.cmd, log=self._log) \
            if self.status == self.SUCCESS else self

    def log(self):
        out_channel = self._out_channel()
        (out, err) = self._log
        if self.input:
            print('*** STDIN ***', file=out_channel)
            print(self.input, file=out_channel)
        if out:
            print('*** STDOUT ***', file=out_channel)
            print(out, file=out_channel)
        if err:
            print('*** STDERR ***', file=out_channel)
            print(err, file=out_channel)

    def repr_success(self):
        return 'OK'

    def repr_failure(self):
        return 'EXIT: {}'.format(self.value)

class ReplayResult(ClientResult):

    @classmethod
    def get(cls, cmd, input=None):
        return (super(ReplayResult, cls).get(cmd, input=input) \
                >> (lambda o: cls.interpret(o, cmd)))

    @classmethod
    def interpret(cls, raw_output, cmd):
        return cls(cls.SUCCESS, cls.parse_output(raw_output.split('\n')), cmd)

    @classmethod
    def parse_output(cls, lines):
        section = None
        michelson_repr = ""
        operations = []
        for line in lines:
            if line == 'storage':
                section = 'storage'
            elif line == 'emitted operations':
                section = 'ops'
            elif section == 'ops':
                operations = cls.parse_operation_details(line.strip(), operations)
            else:
                michelson_repr += line + '\n'
        return (michelson_repr, operations)

    @classmethod
    def parse_operation_details(cls, line, ops):
        if line == 'Internal transaction:':
            ops.append({})
        else:
            try:
                [key, value] = line.split(':')
                ops[-1][key.strip().lower()] = value.strip().replace('ꜩ', '')
            except:
                pass
        return ops

    def log(self):
        out_channel = self._out_channel()
        print("'" + "' '".join(self.cmd) + "'", file=out_channel)
        if self.status == self.FAILURE:
            super(ReplayResult, self).log()
        else:
            (storage, ops) = self.value
            print(storage, file=out_channel)
            print(ops, file=out_channel)

    @property
    def storage_michelson(self):
        return self.map(lambda v: v[0])

    @property
    def operations(self):
        return self.map(lambda v: v[1])


class AssertionResult(Result):

    def __init__(self, status, expected, actual):
        super().__init__(status, actual)
        self.expected = expected

    @classmethod
    def ok(cls, value):
        return cls(cls.SUCCESS, value, value)

    @classmethod
    def fail(cls, expected, actual):
        return cls(cls.FAILURE, expected, actual)

    def log(self):
        status = 'OK' if self.status == self.SUCCESS else 'ERROR'
        print('Assertion({} = {}): {}'.format(self.expected, self.value, status))

    def repr_success(self):
        return 'ASSERTION'

class Assertion:

    def __init__(self, expected):
        self.expected = expected

    def equal(self, actual):
        if self.expected == actual:
            return AssertionResult.ok(actual)
        else:
            return AssertionResult.fail(self.expected, actual)


def parse_args():
    parser = ArgumentParser()
    parser.add_argument('target', help='The address of the contract.')
    parser.add_argument('-o', '--operation', action='append', help='An operation hash of the operation to execute.')
    parser.add_argument('--script', help='A file containing script code to replace the original script.')
    parser.add_argument('--script-from-node', dest='script_from_node', action='store_true', help='Fetch script code from the node.')
    parser.add_argument('--amount', type=float, help='The amount of Tez transferred to the contract account.')
    parser.add_argument('--balance', type=float, help='Balance of the contract account before the transaction.')
    parser.add_argument('--source', help='The source address of the transaction.')
    parser.add_argument('--client', help='The command to run as tezos-client.')
    parser.add_argument('-m', '--mockup', action='store_true', help='Run the client in mockup mode.')
    parser.add_argument('-n', '--node', default='http://localhost:8732', help='URL of the node to connect to.')
    parser.add_argument('-d', '--data-dir', dest='client_dir', help='Client data directory.')
    parser.add_argument('-t', '--trace', action='store_true', help='Print stack state after each instruction.')
    parser.add_argument('-c', '--check', action='store_true', help='Compare obtained results with those stored on chain.')
    parser.add_argument('-q', '--quiet', action='store_true', help='Minimize the produced output to reporting errors.')
    parser.add_argument('-f', '--filter', help='Provide a function from operation to bool in order to filter some operations out.')
    return parser.parse_args()

def find(predicate, l):
    for item in l:
        if predicate(item):
            return item
    return None

def call_api(api, path, **query):
    url = '{}/{}?{}'.format(api, '/'.join(path), parse.urlencode(query))
    response = request.urlopen(url)
    return response.read()

def call_client(cmd, input=None):
    return ClientResult.get(cmd, input=input)

def to_michelson(client_cmd, data):
    return call_client(client_cmd + \
                       ['convert', 'data', data, 'from', 'json', 'to', 'michelson'])

def normalize(client_cmd, michelson, ty):
    return call_client(client_cmd + ['normalize', 'data', michelson, 'of', 'type', ty])

def is_main_transaction(op):
    return op['type'] == 'transaction' \
        and 'initiator' not in op.keys() \
        and op['status'] == 'applied'

def is_internal_wrt(main_op):
    def check(op):
        return main_op['id'] != op['id'] \
            and op['type'] == 'transaction' \
            and op['sender']['address'] == main_op['target']['address']
    return check

def format_tez(mutez):
    return '{:f}'.format(mutez / 1000000.)

def get_balance(address, level):
    return lambda: json.loads(
            call_api(TZKT, ['accounts', address, 'balance_history', str(level)])
        )

def assert_eq(values):
    (expected, actual) = values
    assertion = Assertion(expected)
    return assertion.equal(actual)

def expected_op(op):
    return (op['sender']['address'], op['target']['address'])

def actual_op(op):
    return (op['from'], op['to'])

def check_internal_operations(op, generated_ops):
    ops = json.loads(call_api(TZKT, ['operations', op['hash']], micheline=3))
    expected_ops = set(map(expected_op, filter(is_internal_wrt(op), ops)))
    assertion = Assertion(expected_ops)
    return assertion.equal(set(map(actual_op, generated_ops)))

def check_storage(client_cmd, op, actual_storage):
    script_json = json.loads(
        call_api(
            TZKT,
            ['contracts', op['target']['address'], 'code'],
            level=op['level'] - 1
        ).decode('utf-8')
    )
    storage_type = to_michelson(
        client_cmd,
        json.dumps(script_json[1]['args'][0])
    )

    storage_michelson = to_michelson(client_cmd, op['storage'])

    def parse_storages_for_comparison(**kwargs):
        return Result.both(
            normalize(client_cmd, kwargs['expected'], kwargs['ty']),
            normalize(client_cmd, actual_storage, kwargs['ty'])
        )
    storages_to_check = Result.bind_many(parse_storages_for_comparison,
                                         expected=storage_michelson,
                                         ty=storage_type)

    return storages_to_check >> assert_eq

def check_failed_result(op, result):
    (stdout, stderr) = result._log
    log = '{}\n{}\nFailed as expected!'.format(stdout, stderr)
    return Result.ok(result.value, log=log) \
        if result.is_failure() else \
           Result.fail('Operation succeeded where it was expected to fail!')


def replay(client_cmd, args, op):
    if op['status'] == 'skipped':
        return Result.ok('SKIPPED')

    predecessor = op['level'] - 1
    now = op['timestamp']
    sender = Option(args.source).value(op['sender']['address'])
    amount = Option(args.amount).value(op['amount'])
    balance = Option(args.balance).value(get_balance(args.target, predecessor))
    storage = call_api(TZKT, ['contracts', args.target, 'storage', 'raw'], level=predecessor)
    param = Option(op.get('parameter')).value({'entrypoint': 'default', 'value': MICHELSON_UNIT})

    if args.script_from_node:
        script = call_client(client_cmd + ['get', 'contract', 'code', 'for', args.target])
    elif args.script:
        with open(args.script, 'r') as fp:
            script = Result.ok(fp.read())
    else:
        script_json = call_api(TZKT, ['contracts', args.target, 'code'], level=predecessor).decode('utf-8')
        script = call_client(
            client_cmd + \
            ['convert', 'script', '/dev/stdin', \
             'from', 'json', 'to', 'michelson'],
            input=script_json
        )
    def run_script(**kwargs):
        cmd = client_cmd + \
            ['run', 'script', '/dev/stdin',
             'on', 'storage', kwargs['storage'],
             'and', 'input', kwargs['input'],
             '--self-address', args.target,
             '--amount', format_tez(amount),
             # The balance is from before the transaction and amount isn't added automatically.
             '--balance', format_tez(balance + amount),
             # sender = source for all transactions directly ordered by an implicit account.
             '--payer', sender,
             '--source', sender,
             '--now', now,
             '--level', str(predecessor),
             '--entrypoint', param['entrypoint']] + \
             (['--trace-stack'] if args.trace else [])
        return ReplayResult.get(cmd, input=kwargs['script'])

    result = Result.bind_many(run_script,
                              script=script,
                              storage=to_michelson(client_cmd, storage.decode('utf-8')),
                              input=to_michelson(client_cmd, param['value']))

    if not args.check:
        return result

    if op['status'] == 'applied':
        ops_result = result.operations >> partial(check_internal_operations, op)
        return ops_result >> (lambda _: result.storage_michelson \
                              >> partial(check_storage, client_cmd, op))
    elif op['status'] == 'backtracked':
        return result.operations >> partial(check_internal_operations, op)
    elif op['status'] == 'failed':
        return check_failed_result(op, result)
    else:
        return Result.fail('Unknown operation status: {}.'.format(op['status']))

def main():
    args = parse_args()
    client_cmd = \
        [Option(args.client).or_else(os.getenv('TEZOS_CLIENT')).value('tezos-client')] + \
        Option(args.client_dir).map(lambda d: ['-d', d]).value([]) + \
        Option(args.node).map(lambda n: ['-E', n]).value([]) + \
        Option.some_if(args.mockup, ['-M', 'mockup'], default=[])

    ops = json.loads(call_api(TZKT, ['operations', 'transactions'],
                              target=args.target,
                              select=OP_FIELDS_TO_FETCH,
                              micheline=3))

    ops_filter = Option(args.operation).map(lambda ops: lambda o: o['hash'] in ops)
    custom_filter = Option(args.filter).map(eval)
    filt = Option.any_or_both(
        ops_filter,
        custom_filter,
        lambda f, g: lambda o: f(o) and g(o)
    ).value(None)
    filtered_ops = ops if filt is None else filter(filt, ops)

    exit_status = 0  # flip to 1 upon error
    for op in filtered_ops:
        try:
            r = replay(client_cmd, args, op)
        except Exception as e:
            r = Result.fail(e, log=traceback.format_exc())
        if r.is_failure():
            exit_status = 1
        if args.quiet:
            print('| {} | {} | {} | {} |'.format(
                op['target']['address'],
                op['hash'],
                r.status_repr,
                r))
        else:
            r.log()
    exit(exit_status)


if __name__ == '__main__':
    main()
