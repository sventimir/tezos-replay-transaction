#!/bin/bash
# Check historical transactions to a given contract in some altered reality
# e.g. with a patched script.

if [[ -z "$1" ]]; then
    accounts="-"
else
    accounts="$1"
    shift
fi

failed=0
src="$(dirname "$0")"

for acc in $(cat "$accounts"); do
    "$src/replay-transaction.py" "$acc" -cq "$@"
done

exit $failed
